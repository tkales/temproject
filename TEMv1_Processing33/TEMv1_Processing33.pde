/*
MSE 652 - Transmission Electron Microscopy
 Thomas Ales, Spring 2017 Project
 Diffraction Pattern Simulation Project
 */

// Setup Variables
int windowX = 1000;   // Width, px
int windowY = 550;    // Height, px
color bgColor = color(20, 39, 68);        // A nice dark blue
color textColor1 = color(255, 255, 255);  // White ttouext
PFont titleFont; PFont bodyFont;
int titleSize = 48; int textSize = 16;    // Default Font Sizes

RadioButton latticeType;
// Import controlP5 Library
import controlP5.*;
// Dirty java things
import java.util.*;
ControlP5 cp5;
DropdownList planeOrder;
PVector[] Planes;

void setup() {
  // Set GUI Size and background color
  size(1000, 550);
  background(bgColor);
  frame.setTitle("SAD-DP:Sim v001 | Processing 3.3");
  // Control Object
  cp5 = new ControlP5(this);
  
  // Load Fonts
  titleFont = createFont("titleFont-48.vlw", titleSize);
  bodyFont = createFont("BodyFont-16.vlw", textSize);
  drawText();
  // Add the zone axis input box
  cp5.addTextfield("zoneAxis")
    .setPosition(200, 48)
    .setSize(60,25)
    .setFont(bodyFont)
    .setValue("-1,0,0")
    .setCaptionLabel("");
  // Add Lattice Type Radio Buttons
  latticeType = cp5.addRadioButton("lattice")
                .setPosition(180, 88)
                .setSize(25, 25)
                .addItem("FCC", 1)
                .addItem("BCC", 2)
                .setFont(bodyFont);
  // Default to FCC Lattice
  latticeType.activate("FCC");
  // Add Lattice parameter textField
  cp5.addTextfield("latticeParameter")
    .setPosition(218, 156)
    .setSize(60, 25)
    .setFont(bodyFont)
    .setCaptionLabel("")
    .setValue("4.09");
    
  // Create calculate button
  cp5.addButton("calculateRings")
    .setValue(0)
    .setPosition(5, 275)
    .setCaptionLabel("Calculate Ring Pattern")
    .setSize(125, 30);
   // Create order selection dropdown
   List planeSizes = Arrays.asList("1", "2", "3", "4", "5", "6");
   cp5.addScrollableList("planeOrder")
     .setPosition(275, 200)
     .setSize(40, 75)
     .setBarHeight(25)
     .setItemHeight(25)
     .addItems(planeSizes)
     .setCaptionLabel("");
   cp5.get(ScrollableList.class, "planeOrder").setType(ControlP5.DROPDOWN);
   // Create calculate planes button.
   cp5.addButton("generatePlanes")
     .setValue(0)
     .setPosition(5, 240)
     .setCaptionLabel("Generate Planes")
     .setSize(125, 30);
  
}

void draw() {
   background(bgColor);
   drawText();
   // Create outline of the Diffraction area and draw crosshair.
   drawFrameOutline(500, 25, 476, 476, textColor1, 1);
   stroke(textColor1); noFill(); strokeWeight(1);
   // 24px crosshair centered about 0,0
   line(738, 251, 738, 275); line(726, 263, 750, 263);
}

void drawFrameOutline(int xPosition, int yPosition, int frameWidth, int frameHeight, color frameColor, int frameThickness) {
   // Helper function. Draws a frame
   noFill(); stroke(frameColor); strokeWeight(frameThickness);
   rect(xPosition, yPosition, frameWidth, frameHeight);
}

public void input(String zoneAxis) {
   // Callback for the zoneAxis input textarea. 
}

void controlEvent(ControlEvent theEvent) {
  // DropdownList is of type ControlGroup.
  // A controlEvent will be triggered from inside the ControlGroup class.
  // therefore you need to check the originator of the Event with
  // if (theEvent.isGroup())
  // to avoid an error message thrown by controlP5.

  if (theEvent.isGroup()) {
    // check if the Event was triggered from a ControlGroup
    println("event from group : "+theEvent.getGroup().getValue()+" from "+theEvent.getGroup());
  } 
  else if (theEvent.isController()) {
    println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
    if(theEvent.getController().getName() == "generatePlanes") {
     println("Captured generatePlanes request\n");

     Planes = planeGen(3);
    }
  }
}
// DRAWTEXT
// Redraws all the static text so ControlP5 will work correctly.
public void drawText() {
  textFont(titleFont);
  noStroke(); fill(textColor1);
  text("SAD-DP:Sim", 5, 45);
  textFont(bodyFont);
  text("Enter desired zone axis:", 5, 65);
  text("Select a lattice type:", 5, 105);
  text("Set lattice parameter [Å]:", 5, 175);
  text("Select Highest Calaculated Plane", 5, 217);
}