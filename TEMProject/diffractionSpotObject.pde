/*
MSE 652 - Transmission Electron Microscopy
Thomas Ales, Spring 2017 Project
Diffraction Pattern Simulation Project
tkales [at] iastate [dot] edu
http://derp.thomasal.es/ISU/652/
https://github.com/TKAles/SixFiftyToo

Licensed under the GNU General Public License, version 2.0,
available at: https://github.com/TKAles/SixFiftyToo/blob/master/LICENSE

*/

// This is the DiffractionSpot object. Contains various properties about
// the spot you're trying to generate.
class DiffractionSpot {
  PVector gVector;                           // [hkl] vector for this spot
  float angleToG1 = 0.00;                    // Angle in radians to FIRST_VECTOR (+x,0) 
  boolean keepVisible = false;               // You want me to keep rendering this?
  float planeSpacing = 0.00;                 // Lattice plane spacing with assoc. gVector [m]
  float xOffset = 0.00; float yOffset = 0.00;// x & y offsets relative to the center crosshair
  float pixelMagnitude = 0.00;               // Equivalent Ring Radius for R_i = (L*Lambda) / d_i [px] 
  float currentAlpha = 255;                  // Alpha value for 'pulse' effect
  float previousAlpha = 255;                 // Previous frame alpha value
  int lastPress = 0;
  
// From Matt Kenney:
// Cross (a, b) then dot against zone axis, a is vector you are indexing against
// b is any other vector that you want to index against a.
// If zero, then 0-180*, if negative 181-360*

// Constructor
DiffractionSpot(PVector inputGVector) {
   gVector = inputGVector;
   planeSpacing = LATTICE_PARAM / gVector.mag();
 }

// Returns a PVector that represents the g-vector for this object
PVector returnPlane() {
  return gVector;
}

// Does indexy stuff.
void setIndex() {
  // Index the diffraction pattern against the chosen g-vector
  // angleToG1 = PVector.angleBetween(gVector, FIRST_VECTOR);
  if((FIRST_VECTOR.x==gVector.x)&&(FIRST_VECTOR.y==gVector.y)&&(FIRST_VECTOR.z==gVector.z)) {
    // do nothing
    angleToG1 = 0.0;
  } else if (FIRST_VECTOR.dot(gVector) == 0) {
    angleToG1 = PI;
  } else {
  angleToG1 = acos(FIRST_VECTOR.dot(gVector) / (FIRST_VECTOR.mag() * gVector.mag()));
  }
  println("P: "+angleToG1+" Std:"/*+angleCheck*/);
  // Get the ring radius and calculate the x and y offsets.
  // Map y to -y
  pixelMagnitude = (CAMERA_LENGTH*BEAM_LAMBDA) / planeSpacing;  // Pixels
  xOffset = pixelMagnitude*cos(angleToG1);                      
  yOffset = (-1.0)*pixelMagnitude*sin(angleToG1);
  float oneEightyCheck = ZONE_AXIS.dot(gVector.cross(FIRST_VECTOR));
  if(oneEightyCheck<0) {
    // If the dot product of the cross product, rotate by 180*.
    // Hipster Java Coords: +x --> +y v
    // Math "Standard Position": +x ---> +y ^
    
    yOffset = (-1.0)*yOffset;
    xOffset = (-1.0)*xOffset;
  }
}

// This renders the particular diffractionobject. 
// Function trusts that all values are current.
void renderSpot() {
    float xPos = xOffset + diffWindowXCenter;
    float yPos = yOffset + diffWindowYCenter;
    stroke(color(255,255,255)); noFill();
    ellipse(xPos, yPos, 3, 3);
}

// Returns the plane spacing that this gvector represents
// for the given LATTICE_PARAM
float returnPlaneSpacing() {
  return planeSpacing;
}

float returnXOffset() {
  return xOffset;
}

float returnYOffset() {
  return yOffset;
}
void renderInformation() {
  // This is fucking hacky. We're gonna get the center of the spot
  // and we're gonna draw a box around it, and then we're gonna check if
  // that damned mouse is in that box, and if it is. Uh, render.
  // also check if the 'keepVisible' flag is set to true, if so, render
  // even if the mouse is not in the hitbox.
  float xPos = xOffset + diffWindowXCenter;
  float yPos = yOffset + diffWindowYCenter;
  float xWindowBegin = xPos - 3.0;
  float yWindowBegin = yPos - 3.0;
  float windowWidth = 6.0;
  
  // I heard you like compound logic. This is purely to make the if statement more readable.
  boolean isInsideX = (xWindowBegin < mouseX) && (mouseX < (xWindowBegin + windowWidth));
  boolean isInsideY = (yWindowBegin < mouseY) && (mouseY < (yWindowBegin + windowWidth));
  
  if(((isInsideX == true)&&(isInsideY==true)) || (keepVisible == true)) {
            
      // aptly named, but we have an a e s t h e t i c to keep      
      // the font isn't really hard to read anymore, but i don't feel like messing with it
      textFont(hardToReadFont); textSize(16);
      
      // The label that's made when hovering over a spot ends up being on the line if 
      // it's in QIII (Std Position). This is to relocate the label for any diffraction spot
      // in QIII so it looks better.
      boolean isQuadrant3 = ((this.xOffset<0) && (this.yOffset>0));
      
      // Not in QIII. Draw as usual
      // TODO: Cleanup, refactor using local offset variables that change based on QIII case.
      //       make draw code common.
      if(isQuadrant3 == false) {
        
        stroke(color(255,255,255)); fill(color(0,0,0));
        rect((xPos + 3), (yPos - 35), 70, 30);
        noStroke(); fill(color(255,255,255));
        text("G: "+nf(gVector.x, 1, 0)+nf(gVector.y, 1, 0)+nf(gVector.z, 1, 0), (xPos + 5), (yPos - 20));
        // Shift the plane spacing into Angstroms
        text("d: "+nf((planeSpacing * pow(10, 10)), 1, 3)+" A", (xPos + 5), (yPos - 7));
        // Draw a line!
        strokeWeight(1);
        stroke(this.currentAlpha);
        line(diffWindowXCenter, diffWindowYCenter, xPos, yPos);
      } else {
        // Must be QIII, render shifted label instead.
        stroke(color(255,255,255)); fill(color(0,0,0));
        rect((xPos - 73), (yPos + 5), 70, 30);
        noStroke(); fill(color(255,255,255));
        text("G: "+nf(gVector.x, 1, 0)+nf(gVector.y, 1, 0)+nf(gVector.z, 1, 0), (xPos - 71), (yPos + 18));
        // Shift the plane spacing into Angstroms
        text("d: "+nf((planeSpacing * pow(10, 10)), 1, 3)+" A", (xPos - 71), (yPos + 32));
        // Draw a line!
        strokeWeight(1);
        stroke(this.currentAlpha);
        line(diffWindowXCenter, diffWindowYCenter, xPos, yPos);
      }
      
      // did the user click on the point? if so, set the flag to keep rendering to true.
      if((mousePressed == true) && ((isInsideX == true)&&(isInsideY == true))) {
        // Debounce logic. Otherwise you trigger the flag every frame
        if((millis() - 200) > this.lastPress) {
          this.keepVisible = !this.keepVisible;
          this.lastPress = millis();
        }
      }
        
      
    }
}

// I made this dumb effect for some reason.
void pulse(int lowerValue, int upperValue, float period) {
  float dVdt = (upperValue - lowerValue) / period;     // How much the alpha should change per frame.
  float frameTime = 1/frameRate;                       // How long the last frame took in seconds.
  float delta = dVdt * frameTime;                      // How much the thing should change.
  float change = this.currentAlpha - this.previousAlpha;
  this.previousAlpha = this.currentAlpha;
  // There is probably a better way to do this
  if(change == 0) {
   if(this.currentAlpha == upperValue) {
      this.currentAlpha = this.currentAlpha - delta;
   } else if(this.currentAlpha == lowerValue) {
      this.currentAlpha = this.currentAlpha + delta;
   }
  } else if(change>0) {
    if(this.currentAlpha<upperValue) {
      this.currentAlpha = this.currentAlpha + delta;
    } else { 
      this.currentAlpha = this.currentAlpha - delta;
    }
  } else if(change<0) {
    if(this.currentAlpha>lowerValue) {
      this.currentAlpha = this.currentAlpha - delta;
    } else if (this.currentAlpha<lowerValue) {
      this.currentAlpha = this.currentAlpha + delta;
    }
  }
}
}