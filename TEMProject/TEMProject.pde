/*
MSE 652 - Transmission Electron Microscopy
Thomas Ales, Spring 2017 Project
Diffraction Pattern Simulation Project
tkales [at] iastate [dot] edu
http://derp.thomasal.es/ISU/652/
https://github.com/TKAles/SixFiftyToo

Licensed under the GNU General Public License, version 2.0,
available at: https://github.com/TKAles/SixFiftyToo/blob/master/LICENSE

*/


// Setup Variables
int windowX = 1000;   // Width, px
int windowY = 550;    // Height, px
color bgColor = color(20, 39, 68);        // A nice dark blue
color textColor1 = color(255, 255, 255); // White text
color frameStrokeColor = color(255, 255, 255); // Frame Color
PFont titleFont; PFont bodyTextFont; PFont frameFont;
PFont hardToReadFont; int badSize = 10;
int titleSize = 48; int textSize = 16; int frameFontSize = 12;    // Default Font Sizes

int DEBUG = 1;

// Import controlP5 Library and declare other CP5-related goodies
import controlP5.*;
ControlP5 cp5;
RadioButton latticeType;

DropdownList planeOrder;

// Dirty java things
import java.util.*;

// Global Flags
int LATTICE_TYPE = 0;        // Lattice Type 0 - FCC, 1 - BCC
float LATTICE_PARAM = 0.00;  // Lattice Parameter
float CAMERA_LENGTH = 0.00;  // Camera Length, meters
float BEAM_LAMBDA = 2.51E-12;            // Beam Wavelength, meters
PVector ZONE_AXIS = new PVector(0,0,0);  // Zone axis, uvw
int VECTOR_ORDER = 1;                    // Highest n for hkl generation
int FIRST_RING_RADIUS = 322;             // Maximum Ring Radius for 1st ring
int REPORTED_DPI = 96;                   // Reported device pixel density, assume wrong
float CAMERA_LENGTH_INCHES = 0.00;         // CAMERA_LENGTH / REPORTED_DPI, also assume wrong
float DOT_TOLERANCE = 0.0001;              // Tolerance for the dot product to be considered zero

float diffWindowXCenter = 864;           // Diffraction Frame Center, x-coord
float diffWindowYCenter = 350;           //      "        "      "  , y-coord
// Need to create a global array of DiffractionSpot(s) to render
ArrayList<DiffractionSpot> diffractionObjects = new ArrayList<DiffractionSpot>();
// Global array of spots that have been 'left on'
ArrayList<DiffractionSpot> visibleSpots = new ArrayList<DiffractionSpot>();
// The PVector you are indexing against, set to 000 until initalized by calculation
PVector FIRST_VECTOR = new PVector(0,0,0); 
// Global Angle Array
FloatList anglesToFirst = new FloatList();
// SETUP: Runs once, before sketch. 
void setup() {
    // Set GUI Size and background color. Select Processing 2D Renderer.
    size(1200, 700, P2D);
    background(bgColor);
    // Set title of window
    surface.setTitle("SAD-DP:Sim v001 | Processing 3.3");
    // Spawn ControlP5 Object
    cp5 = new ControlP5(this);
    // Set Processing's method for specifying an ellipse to radius mode
    ellipseMode(RADIUS);
    
    // Load Fonts
    hardToReadFont = loadFont("hardToReadFont.vlw");
    titleFont = loadFont("Oswald-Medium-48.vlw");
    bodyTextFont = loadFont("Oswald-Regular-14.vlw");
    frameFont = loadFont("Oswald-Medium-12.vlw");
    
    // Draw the initial frame text, decorations, etc.
    drawText();

/*                             */
// CONTROLP5 Setup Stuff Start //
/*                             */

    // Add the zone axis input box
    cp5.addTextfield("zoneAxis")
      .setPosition(180, 122)
      .setSize(60,25)
      .setValue("-1,0,0")
      .setCaptionLabel("");
    // Add the zone axis input box
    cp5.addTextfield("cameraLength")
      .setPosition(180, 151)
      .setSize(60,25)
      .setValue("0000")
      .setCaptionLabel("");      
    // Add Lattice Type Radio Buttons
    latticeType = cp5.addRadioButton("lattice")
      .setPosition(180, 208)
      .setSize(25, 25)
      .addItem("FCC", 0)
      .addItem("BCC", 1);
    
    // Default to FCC Lattice
    latticeType.activate("FCC");
    
    // Add Lattice parameter textField
    cp5.addTextfield("latticeParameter")
      .setPosition(180, 270)
      .setSize(60, 25)
      .setCaptionLabel("")
      .setValue("4.09");
    
     // Create order selection dropdown
     cp5.addTextfield("planeOrder")
      .setPosition(180, 300)
      .setSize(60, 25)
      .setCaptionLabel("")
      .setValue("3");
     
     // Create calculate planes button.
     cp5.addButton("generatePlanes")
       .setValue(0)
       .setPosition(5, 400)
       .setCaptionLabel("Calculate")
       .setSize(125, 30);
     
    
/*                             */
// CONTROLP5 Setup Stuff End   //
/*                             */
    noStroke();
    // Do inital render for defaults.
    generatePlanes();
}
// DRAW: Runs 60fps, should execute within 16.2ms, if not, spawn 
//       a thread otherwise you're gonna start locking things up.

void draw() {

  // Reset Surface BG
   background(bgColor);
   // Redraw text
   drawText();
   
   // Blank Diffraction Surface, then create outline of the Diffraction area and draw crosshair.
   noStroke(); fill(bgColor); rect(539, 25, 1189, 675);
   drawFrameOutline(539, 25, 650, 650, textColor1, 1);
   stroke(textColor1); noFill(); strokeWeight(1);
   
   // 24px crosshair centered about 0,0
   // TODO: This looks bigger than Donald's hands. Maybe 12-16px?
   line(864, 338, 864, 362); line(852, 350, 876, 350);
   
   // Draw Organizing Frames (title, x, y, width, height, titleClearance)
   drawContainerFrame("CAMERA INFO", 68, 5, 112, 275, 75);
   drawContainerFrame("LATTICE PARAMS", 83, 5, 200, 276, 137);
   
   // Check to see if the diffractionObjects array has DiffractionSpots in it.
   if(diffractionObjects.size()>0) {
     // Iterate through the ArrayList. 
     ArrayList<DiffractionSpot> renderedSpots = new ArrayList<DiffractionSpot>();
     for(int i = 0; i < diffractionObjects.size(); i++) {
      // create a temp DS, and load a DS from the array into it
      DiffractionSpot tempSpot = diffractionObjects.get(i);
      // index it against FIRST_VECTOR
      tempSpot.setIndex();
      // render the spot itself, relative to the crosshair
      tempSpot.renderSpot();
      // render the information window and draw a line. so you can see.
      tempSpot.renderInformation();
      // Should we keep rendering this? If so turn off the pulse, because
      // that getting out of synch tweaks me for some reason.
      if(tempSpot.keepVisible == true) {
        // Change Spots' Whiteness value (this is basically 255 White - 0 Black), not alpha.
        // TODO: fix. not gonna write my own alpha code.
        tempSpot.currentAlpha = 255;
        // Render the line and info window.
        tempSpot.renderInformation();
        renderedSpots.add(tempSpot);
      }
    tempSpot.pulse(125, 255, 2); //<>// //<>//
    }
   }
   generateInformationTable(5,450,diffractionObjects);
}
// This is a controlP5 callback just hanging out for no reason
// I'm not sure why I just wrote comments about it instead of deleting it.
public void input(String zoneAxis) {
  // Callback for the zoneAxis input textarea. 
}

// Also not used. Notifies when BCC/FCC lattice is switched. Lazy.
void controlEvent(ControlEvent theEvent) {
  // DropdownList is of type ControlGroup.
  // A controlEvent will be triggered from inside the ControlGroup class.
  // therefore you need to check the originator of the Event with
  // if (theEvent.isGroup())
  // to avoid an error message thrown by controlP5.
}  // Terminates controlEvent Function


// Logic for when the "Generate Planes" button is pressed.
public void generatePlanes() {
  generateAndCheckVectors();    // Wonder what this does
}

// GENERATEANDCHECKVECTORS:
//      Expects: Nothing. ಠ_ಠ
//      Returns: Nothing. ಠ_ಠ
//      What does it even do then? GENERATEANDCHECKVECTORS, sets all applicable
//      globals to whatever the current values in the GUI are set to. Generates all planes from [-n-n-n]:[nnn]
//      inclusive where N = PLANE_ORDER. Calculates optimal camera length and updates the global. Then 
//      checks generated vectors against the lattice extinction rules. Further checks against g dot B = 0 where g is the
//      generated g-vector, and B is the selected zone axis.

public void generateAndCheckVectors() {
   // Clear out the global diffractionSpot array.
   diffractionObjects.clear();
   
   // First update all inputs to the global input
   // Pull zone-axis string from textbox, split along commas and update ZONE_AXIS global
   String zoneAxisInput = cp5.get(Textfield.class,"zoneAxis").getText();
   String[] zoneSplit = zoneAxisInput.split(",");                         
   ZONE_AXIS.set(int(zoneSplit[0]), int(zoneSplit[1]), int(zoneSplit[2]));
   
   // Pull lattice parameter from textbox as well, shift to meters and update
   // corresponding global
   String latticeParamInput = cp5.get(Textfield.class,"latticeParameter").getText();
   LATTICE_PARAM = float(latticeParamInput) * pow(10, -10);
   
   // Update lattice type information. FCC = 0, BCC = 1.
   if(latticeType.getState(0) == true) {
     LATTICE_TYPE = 0;
   } else if(latticeType.getState(1) == true) {
     LATTICE_TYPE = 1;
   }
   
   // Update plane order
   VECTOR_ORDER = int(cp5.get(Textfield.class,"planeOrder").getText());
   
   if(DEBUG==1) { 
     println("ZONE_AXIS updated to: "+zoneSplit[0]+zoneSplit[1]+zoneSplit[2]+" direction.");
     println("LATTICE_PARAM updated to:"+LATTICE_PARAM+" meters.");
     println("LATTICE_TYPE updated to: "+LATTICE_TYPE);
     println("VECTOR_ORDER set to: "+VECTOR_ORDER);
   }
   
   // Calculate all planes
   ArrayList<PVector> allPlanesGenerated = new ArrayList<PVector>();
   allPlanesGenerated = planeGenerator(VECTOR_ORDER);
   // Run extinction ruleset for lattice type
   ArrayList<PVector> allowedPlanesForLattice = new ArrayList<PVector>();
   allowedPlanesForLattice = checkExtinction(LATTICE_TYPE, allPlanesGenerated);
   // Run intersection check
   ArrayList<PVector> intersectingWithZoneAxis = new ArrayList<PVector>();
   intersectingWithZoneAxis = checkZone(ZONE_AXIS, allowedPlanesForLattice);

   if(DEBUG == 1) {
     println("Generated "+allPlanesGenerated.size()+" vectors.");
     println("Extinction ruleset "+LATTICE_TYPE+" reduced to "+allowedPlanesForLattice.size());
     println("Dot Product check reduced allowed vectors to "+intersectingWithZoneAxis.size()+" vectors.");
   }
   
   // Find the smallest plane spacing.
   int smallestValue = findClosest(intersectingWithZoneAxis);
   PVector smallestPlane = intersectingWithZoneAxis.get(smallestValue);
   float smallestMagnitude = LATTICE_PARAM / smallestPlane.mag();
   
   if(DEBUG==1) { 
     println("Smallest Value is at array index "+smallestValue);
     println("Corresponding Miller Index: "+smallestPlane.x+smallestPlane.y+smallestPlane.z);
     println("Planar spacing: "+smallestMagnitude);
   } //<>// //<>//
   
   // Calculate what the camera length should be and update the GUI.
   CAMERA_LENGTH = (FIRST_RING_RADIUS * (smallestMagnitude/1.3)) / BEAM_LAMBDA;
   cp5.get(Textfield.class, "cameraLength").setText(str(round(CAMERA_LENGTH)));
   
   // Load the smallestPlane into the global diffractionObject array and set as FIRST_VECTOR
   diffractionObjects.add(new DiffractionSpot(smallestPlane));
   FIRST_VECTOR = smallestPlane;
   
   // Iterate through the ArrayList, skipping the vector you used for the FIRST_VECTOR
   for(int i = 0; i<intersectingWithZoneAxis.size(); i++) {
     if(i == smallestValue) { /* This was used for FIRST_VECTOR, skip */ } 
     // Add the rest of the spots to the global array.
     else { diffractionObjects.add(new DiffractionSpot(intersectingWithZoneAxis.get(i))); }
   }
   
   if(DEBUG==1) { println("Created "+diffractionObjects.size()+" diffractionObjects."); }
   
}