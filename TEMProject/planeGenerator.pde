/*
MSE 652 - Transmission Electron Microscopy
Thomas Ales, Spring 2017 Project
Diffraction Pattern Simulation Project
tkales [at] iastate [dot] edu
http://derp.thomasal.es/ISU/652/
https://github.com/TKAles/SixFiftyToo

Licensed under the GNU General Public License, version 2.0,
available at: https://github.com/TKAles/SixFiftyToo/blob/master/LICENSE

*/

// Plane generator function.
// Inputs: Highest Order Value [int]
// Returns: ArrayList of PVectors
// Takes the Highest Order Value and calculates all possible HKL planes for given order
public ArrayList planeGenerator(int theValue) {
  ArrayList<PVector> outputPlanes = new ArrayList<PVector>();
  for(int h=(-1*theValue); h<theValue+1; h = h+1) {
    for(int k=(-1*theValue); k<theValue+1; k = k+1) {
      for(int l=(-1*theValue); l<theValue+1; l = l+1) {
        PVector currentPlane;
        currentPlane = new PVector(h, k, l);
        outputPlanes.add(currentPlane);
      }
    }
  }
  return outputPlanes;
}

// Allowed Reflections Conditioner
// Inputs: Lattice Type Designator [int], ArrayList of PVectors
//         Lattice Type Key: 0 - FCC, 1 - BCC
// Returns: ArrayList of PVectors corresponding to allowed reflections for given lattice
public ArrayList checkExtinction(int latticeType, ArrayList<PVector> planesToCheck) {
  ArrayList<PVector> checkedPlanes = new ArrayList<PVector>();
  // Lattice Types: 0 - FCC, 1 - BCC
  // For FCC: Reflection Allowed when H, K & L are all odd or all even
  // For BCC: Reflection Allowed when H+K+L = even
  // Get size of vector array
  int listSize = planesToCheck.size();
  if(latticeType == 0) {
    // Check each vector component for odd or even ness
    boolean hEven; boolean kEven; boolean lEven;
    // Iterate through arrayList
    for(int i = 0; i<listSize; i++) {
      PVector tempVector = planesToCheck.get(i);
      hEven = tempVector.x % 2 == 0;
      kEven = tempVector.y % 2 == 0;
      lEven = tempVector.z % 2 == 0;
      // Check if all hkl are even
      if((hEven==true)&&(kEven==true)&&(lEven==true)) {
      checkedPlanes.add(tempVector);
      }
      // Check if all hkl are odd
      if((hEven!=true)&&(kEven!=true)&&(lEven!=true)) {
      checkedPlanes.add(tempVector);
      }    
      } // Terminates for loop for ArrayList
    } // Terminates if(latticeType == 0)
  if(latticeType == 1) {
      // Now for BCC, H+K+L=even or (H+K+L) modulo 2 = 0 == true
      // Iterate through the array and check the magnitude. 
      for(int i = 0; i<listSize; i++) {
        PVector tempVector = planesToCheck.get(i);
        float vectorSum = tempVector.x + tempVector.y + tempVector.z;
        if((vectorSum % 2) == 0) {
          checkedPlanes.add(tempVector);
        }
      }
  }
  return checkedPlanes;
}

// Plane spacing calculator
// Inputs: Maximum Ring Distance Allowed [int/px], Beam Energy [float/m], 
//         g-vector array [PVectors]
// Output: Float array corresponding to planar distance.
public float[] calculatePlaneSpacing(ArrayList<PVector> inputArray, float latticeParameter) {
  // Get size of the incoming vector array
  int sizeOfArray = inputArray.size();
  // Allocate a float array the size of the vector array
  float planes[] = new float[sizeOfArray];
  for(int i = 0; i<sizeOfArray; i++) {
    PVector tempVector = inputArray.get(i);
    planes[i] = latticeParameter / tempVector.mag();
   }
  return planes;
}

// Zone axis filter
// Inputs: Zone Axis [<PVector>], ArrayList<PVectors> of Canidate planes
// Output: ArrayList of PVectors that satisfy the Weiss Zone Law
//         hu + kv + lw = 0, aka [uvw] dot [hkl] = 0
public ArrayList<PVector> checkZone(PVector inputZoneAxis, ArrayList<PVector> inputGVectors) {
  // Get size of incoming array  
  int sizeOfGVectors = inputGVectors.size();
  // Setup return array
  ArrayList<PVector> outputVectors = new ArrayList<PVector>();
  // Iterate over incoming loop
  for (int i=0; i<sizeOfGVectors; i++) {
    PVector tempVector = inputGVectors.get(i);
    float dotProduct = inputZoneAxis.dot(tempVector);
    if(((0.0-DOT_TOLERANCE) < dotProduct) && (dotProduct < (0.0+DOT_TOLERANCE))) {
      if((tempVector.x == 0.0) && (tempVector.y == 0.0) && (tempVector.z == 0.0)) {
        // Do nothing
        if(DEBUG == 1) { println("Found the 000 Vector. Discarding."); }
      } else {
        outputVectors.add(tempVector);
      }
    }
  }
  return outputVectors;
}

// FINDCLOSEST: Returns smallest plane spacing, and therefore largest ring radius
//              used in the autoscale system and camera length calculations. 
//              Does not update any globals.
//              Inputs: ArrayList of PVectors that represent g-vectors
//                      Assumes LATTICE_PARAM is set to something useful.
//              Returns: int that represents index of PVector array the smallest value
//                       was found at.
public int findClosest(ArrayList<PVector> inputVectors) {
  int sizeOfInput = inputVectors.size();
  float smallestMag = 0.0;
  int smallestMagIndex = 0;
  PVector smallestPlane = new PVector();
  for(int i = 0; i<sizeOfInput; i++) {
      PVector tempVector = inputVectors.get(i);
      float planeSpacing = LATTICE_PARAM / tempVector.mag();
      if(planeSpacing < smallestMag) {
        smallestMag = planeSpacing;
        smallestPlane = tempVector;
        smallestMagIndex = i;
      }
  }
  return smallestMagIndex;
}

// You should like work on this. So it's useful. 
// Y'know?

void renderFirstVector(int scaleSize) {
  // This function draws a cute little arrow with the g-vector 
}