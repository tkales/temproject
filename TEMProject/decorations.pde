/*
MSE 652 - Transmission Electron Microscopy
Thomas Ales, Spring 2017 Project
Diffraction Pattern Simulation Project
tkales [at] iastate [dot] edu
http://derp.thomasal.es/ISU/652/
https://github.com/TKAles/SixFiftyToo

Licensed under the GNU General Public License, version 2.0,
available at: https://github.com/TKAles/SixFiftyToo/blob/master/LICENSE

*/

// drawContainerFrame

public void drawContainerFrame(String phrase, int blackoutWidth, int xPosition, int yPosition, int xDelta, int yDelta) {
  // draw the frame
  strokeWeight(1); stroke(frameStrokeColor);
  noFill();
  rect(xPosition, yPosition, xDelta, yDelta);
  // Write Text
  noStroke(); fill(bgColor);
  rect((xPosition + 5), (yPosition - 5), blackoutWidth, 15);
  fill(color(255,255,255));
  textAlign(LEFT); textSize(frameFontSize);
  text(phrase, xPosition + 10, yPosition + 5);
}

// drawText
// Redraws all the static text so ControlP5 will work correctly.
public void drawText() {
  textFont(titleFont);
  textSize(titleSize);
  noStroke(); fill(textColor1);
  
  // Title
  textAlign(LEFT);
  text("M S E 652 DIFFRACTION", 5, 45);
  text("PATTERN SIMULATOR", 5, 88);
  
  // Labels for textboxes
  noStroke(); fill(textColor1);
  textFont(bodyTextFont);
  textSize(14); textAlign(RIGHT);
  text("Zone Axis Vector [u,v,w]", 176, 139);
  text("Simulated Camera Length [px]", 176, 168);
  text("Lattice Type", 176, 224);
  text("Cell Length [Å]", 176, 286);
  text("Highest hkl value", 176, 317);
}

void drawFrameOutline(int xPosition, int yPosition, int frameWidth, int frameHeight, color frameColor, int frameThickness) {
   // Helper function. Draws a frame
   noFill(); stroke(frameColor); strokeWeight(frameThickness);
   rect(xPosition, yPosition, frameWidth, frameHeight);
}

void generateInformationTable(int xPosition, int yPosition, ArrayList<DiffractionSpot> spotList) {
  int l1Offset = xPosition + 55;
  int l2Offset = xPosition + 159;
  int l3Offset = xPosition + 212;
  stroke(color(255,255,255)); noFill();
  rect(xPosition, yPosition, 274, 36);
  textFont(hardToReadFont); textSize(16);
  text("g", (xPosition + 26), (yPosition + 21));
  line(l1Offset, (yPosition + 0), l1Offset, (yPosition + 35));
  text("d [Angstroms]", (xPosition + 67), (yPosition + 21));
  line(l2Offset, (yPosition + 0), l2Offset, (yPosition + 35));
  text("R [px]", (xPosition + 170), (yPosition + 21));
  line(l3Offset, (yPosition + 0), l3Offset, (yPosition + 35));
  text("Angle to", (xPosition + 220), (yPosition + 16));
  text(" "+nf(FIRST_VECTOR.x,1,0)+nf(FIRST_VECTOR.y,1,0)+nf(FIRST_VECTOR.z,1,0)+" ",
    (xPosition + 232), (yPosition + 30));
  int j = 1;
  for(int i = 0; i<spotList.size();i++) {
     DiffractionSpot currentSpot = spotList.get(i);
     if(currentSpot.keepVisible == true) {
       float thisY = yPosition + 36.0*j;
       rect(xPosition, thisY, 274, 36);
       line(l1Offset, thisY, l1Offset, (thisY+36.0));
       line(l2Offset, thisY, l2Offset, (thisY+36.0));
       line(l3Offset, thisY, l3Offset, (thisY+36.0));
       j++;
       textAlign(CENTER);
       PVector currentPlane = currentSpot.returnPlane();
       text(" "+nf(currentPlane.x,1,0)+nf(currentPlane.y,1,0)+nf(currentPlane.z,1,0)+" ", xPosition + 28, thisY + 24);
       text(" "+nf((currentSpot.planeSpacing * pow(10, 10)), 1, 3)+" ", xPosition + 108, thisY + 24);
       text(" "+round(currentSpot.pixelMagnitude)+" ", xPosition + 188, thisY + 24);
       text(" "+nf(degrees(currentSpot.angleToG1),2,2)+ " *", xPosition + 248, thisY + 24);
     }
    }
  }