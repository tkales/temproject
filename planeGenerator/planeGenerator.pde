import controlP5.*;
ControlP5 cp5;
int planeOrder = 5;
boolean dbMode = true;
Textarea planesGenerated;
String planeList;
int sizeOfOutputPlanes;

void setup() {
  size(350,350);
  cp5 = new ControlP5(this);
  
  cp5.addButton("calculate")
    .setValue(2)
    .setPosition(25,25)
    .setSize(125, 25)
    .setCaptionLabel("Caclulate Planes");

planesGenerated = cp5.addTextarea("planes")
    .setPosition(100,100)
    .setSize(200,200)
    .setFont(createFont("arial",12))
    .setLineHeight(14)
    .setColor(color(128))
    .setColorBackground(color(255,100))
    .setColorForeground(color(255,100));
}

void draw() {
    
}

public void controlEvent(ControlEvent theEvent) {
  println(theEvent.getController().getName());
  if(theEvent.getController().getName() == "calculate") {
    ArrayList<PVector> rawPlanes = new ArrayList<PVector>();
    rawPlanes = generatePlanes(int(theEvent.getValue()));
    String textAreaUpdate;
    int rawSize = rawPlanes.size();
    textAreaUpdate = "No. of Generated Planes: " + rawSize + "\n";
    ArrayList<PVector> allowedPlanes = checkExtinction(0, rawPlanes);
    int allowedSize = allowedPlanes.size();
    textAreaUpdate = textAreaUpdate + "No. of Allowed Planes: " + allowedSize + "\n";
    for(int i = 0; i<allowedSize; i++) {
      PVector tempVector = allowedPlanes.get(i);
      textAreaUpdate = textAreaUpdate + "Plane No. " + i + ": [" + tempVector.x +
                        "," + tempVector.y + "," + tempVector.z + "]\n";
    }
    planesGenerated.clear();
    planesGenerated.setText(textAreaUpdate);
}
}

public ArrayList checkExtinction(int latticeType, ArrayList<PVector> planesToCheck) {
   ArrayList<PVector> checkedPlanes = new ArrayList<PVector>();
   // Lattice Types: 0 - FCC, 1 - BCC
   // For FCC: Reflection Allowed when H, K & L are all odd or all even
   // For BCC: Reflection Allowed when H+K+L = even
   // Get size of vector array
   int listSize = planesToCheck.size();
   if(latticeType == 0) {
      // Check each vector component for odd or even ness
      boolean hEven; boolean kEven; boolean lEven;
      // Iterate through arrayList
      for(int i = 0; i<listSize; i++) { //<>//
       PVector tempVector = planesToCheck.get(i);
       hEven = tempVector.x % 2 == 0;
       kEven = tempVector.y % 2 == 0;
       lEven = tempVector.z % 2 == 0;
       // Check if all hkl are even //<>//
       if((hEven==true)&&(kEven==true)&&(lEven==true)) {
         checkedPlanes.add(tempVector);
       }
       // Check if all hkl are odd
       if((hEven!=true)&&(kEven!=true)&&(lEven!=true)) {
         checkedPlanes.add(tempVector);
       }    
      } // Terminates for loop for ArrayList
   } // Terminates if(latticeType == 0)
   if(latticeType == 1) {
    // Now for BCC, H+K+L=even or (H+K+L) modulo 2 = 0 == true
    // Iterate through the array and check the magnitude. 
    for(int i = 0; i<listSize; i++) {
      PVector tempVector = planesToCheck.get(i);
      int vectorMag = int(tempVector.mag());
      boolean sumEven = vectorMag % 2 == 0;
      if(sumEven == true) {
        checkedPlanes.add(tempVector);
      }
    }
   }
   
   return checkedPlanes;
}

public void calculate() {
   
}

public ArrayList generatePlanes(int theValue) {
  ArrayList<PVector> outputPlanes = new ArrayList<PVector>();
   for(int h=0; h<theValue+1; h = h+1) {
     for(int k=0; k<theValue+1; k = k+1) {
       for(int l=0; l<theValue+1; l = l+1) {
         PVector currentPlane;
         currentPlane = new PVector(h, k, l);
         outputPlanes.add(currentPlane);
         // 
       }
     }
   }
   println(outputPlanes.size() + " Raw Limit \n");
   return outputPlanes;
}