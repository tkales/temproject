MSE 652 - Transmission Electron Micrscopy  
Spring 2017  
Semester Project  
Thomas Ales  
  
Given:		 Lattice Type & Parameter  
		 Zone Axis  
  
Generate:	 Simulated Diffraction Pattern  
		 Calculate Camera Length  
		 Label Pattern and Index  
  
Restrictions:	 Single Element, Single Crystal. Force Lattice Selection.  
  
  
Bonuses:	 Animate Rotations  
		 Allow Second Crystal?  
  
First Objective:  
Generate a ring pattern on request when given a zone axis, lattice type & parameter  
  
Second:  
Fork from Ring Pattern, use plane generator developed to get ring patterns with logic for diffraction spots.  
Project Requirements should be satisfied with 2nd objective if I am correctly understanding project req's.  
  
~ Bonus Rounds ~  
Third:  
Interrogate OS applet is running on for screen dimensions + resolution. Calculate PPI and give 'realistic'*  
camera lengths and length correct measurements.  
* - unless the OS is a dirty liar. Which has happened before.  
  
Fourth:  
Use float miller indices (the dirty truth is you were using them all along #madlad) to get relative intensity values and   adjust accordingly.  
  
Fifth:  
Animate zone axis traversal with SAD pattern correctly simulated.   
* Create 2nd worker thread for plane calcs.